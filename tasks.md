Gotify: 

  - [ ] Change creds of `admin:admin` to something else

PostgreSQL:

  - [ ] Switch from `:14-alpine` to `:15-alpine`

Nextcloud:

  - [ ] 1. Go to [Settings > Administration > Overview](https://cloud.thefossguy.com/settings/admin/overview) and check for:

    - Security & setup warnings
    - Version updates

  - [ ] 2. Install the following [apps](https://cloud.thefossguy.com/settings/apps):

    - Default encryption module
    - End-to-End Encryption
    - Two-Factor TOTP Provider

  - [ ] 3. **IMPORTANT** Go to [Settings > Administration > Security](https://cloud.thefossguy.com/settings/admin/security) and

    - Enable `Server-side encryption` using the "Default encryption module"
    - Enable `Encrypt the home storage`

  - [ ] 4. Log out and log back in.

  - [ ] 5. Go to [Settings > Administration > Usage survey](https://cloud.thefossguy.com/settings/admin/survey_client) and untick `Send usage survey monthly`.

  - [ ] 6. Go to [Settings > Personal > Security](https://cloud.thefossguy.com/settings/user/security) and enable "TOTP (Authenticator app)".

  - [ ] 7. Go to [Settings > Personal > Notifications](https://cloud.thefossguy.com/settings/user/notifications) and enable _push notifications_ for the following activities:

    - Your group memberships were modified
    - Your password or email was modified
    - Security
    - TOTP (Authenticator App)

  - [ ] 8. Go to [Settings > Administration > Basic Settings](https://cloud.thefossguy.com/settings/admin) and change change "Background Jobs" to "Cron".

  - [ ] 9. Install the following [apps](https://cloud.thefossguy.com/settings/apps):

    - Activity
    - Auditing/Logging
    - Brute-force settings
    - Calendar
    - Contacts
    - Contacts Interaction
    - Deck
    - Deleted files
    - File sharing
    - Log Reader
    - Monitoring
    - Nextcloud announcements
    - Notes
    - Notifications
    - PDF Viewer
    - Photos
    - Privacy
    - Text
    - Versions

  - [ ] 10. _Disable_ the following [apps](https://cloud.thefossguy.com/settings/apps):

    - Dashboard
    - External storage support
    - LDAP user and group backend
    - OpenOTP Two Factor Authentication
    - Recommendations
    - Usage survey

Gitea:

  - [ ] [Enable Two-Factor Authentication](https://git.thefossguy.com/user/settings/security)

  - [ ] Set up a push mirror to [GitHub](https://docs.gitea.io/en-us/repo-mirror/#setting-up-a-push-mirror-from-gitea-to-github) and [GitLab](https://docs.gitea.io/en-us/repo-mirror/#setting-up-a-push-mirror-from-gitea-to-gitlab). Make sure that the "Sync when commits are pushed" is checked. Use the default 8 hour sync interval.

  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
  - [ ] 
